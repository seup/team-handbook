Coding Style
============

Embedded Coding Style 
---------------------
We will be using `Linux kernel coding style <https://github.com/torvalds/linux/blob/master/Documentation/process/coding-style.rst>`_ with the exception of indentation. We will use 4 spaces instead of 8.


General Coding Style
--------------------

* `Google Style Guides <https://github.com/google/styleguide>`_