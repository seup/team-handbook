Welcome to SEUP Team Handbook (STH)
===================================

The SEUP team handbook is a central repository for how we manage our projects. SEUP stands for `Simple Electric Utility Platform (SEUP) <https://seupdocs.readthedocs.io>`_. SEUP is a project at the University of Wisconsin-Madison where we are building tools for energy management and education.

If you want to contribute to this handbook, please check the `Contributing guide <https://gitlab.com/seup/team-handbook/blob/master/CONTRIBUTING.md>`_.

Please create a `merge request <https://gitlab.com/seup/team-handbook/merge_requests>`_ to suggest improvements or add clarifications (you need to fork the project and then create a merge request. See `this <https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html>`_ for more details). Please use `issues <https://gitlab.com/seup/team-handbook/issues>`_ to ask questions.

A PDF version of this manual can be found `here <https://gitlab.com/seup/team-handbook/blob/master/TeamHandbook.pdf>`_.

The document source can be found `here <https://gitlab.com/seup/team-handbook>`_.

*Why SDH?*

* More professional approach to product development and maintenance
* Follow good industry practices 
* Better project management
* Good documentation for users, current and future developers
* Aligning project for potential community (or large team) collaboration 
* Increase product reliability and performance through better pre (definition and requirement specs) and post (testing) product development practices.
* Allow addition of `continuous integration (CI) and continuous deployment (CD) <https://www.digitalocean.com/community/tutorials/an-introduction-to-continuous-integration-delivery-and-deployment>`_ of SEUP software for better testing and generating builds.

-----

**Contents**:

.. toctree::
   :maxdepth: 2

   projectManage
   repo
   productDev
   documentation
   other
   codeReview
   testing





.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`search`


Bugs? Questions?
================
Raise an issue `here <https://gitlab.com/seup/team-handbook>`_.


Want to contribute?
===================
Check out the `Contributing guide <https://gitlab.com/seup/team-handbook/blob/master/CONTRIBUTING.md>`_


Admin and Project Maintainer
============================
Ashray Manur
ashraymanur@gmail.com