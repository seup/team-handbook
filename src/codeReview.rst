Code Review
===========

Embedded Code Review
--------------------

If you're writing embedded code, this section is for you. This checklist was adapted from `here <https://users.ece.cmu.edu/~koopman/pubs/code_review_checklist_v1_00.pdf>`_

#. **Function**
	* Does the code match the design and the system requirements?
	* Does the code do what it should be doing?
	* Does the code do anything it should not be doing?
	* Can the code be made simpler while still doing what it needs to do?
	* Are available building blocks used when appropriate? (algorithms, data structures, types, templates, libraries, RTOS functions)
	* Does the code use good patterns and abstractions? (e.g., state charts, no copy-and paste)
	* Can this function be written with a single point of exit? (no returns in middle of function)
	* Are all variables initialized before use?
	* Are there unused variables?
	* Is each function doing only one thing? (Does it make sense to break it down into smaller modules that each do something different?)
#. **Style**
	* Does the code follow the style guide for this project?
	* Is the header information for each file and each function descriptive enough?
	* Is there an appropriate amount of comments? (frequency, location, and level of detail)
	* Is the code well structured? (typographically and functionally)
	* Are the variable and function names descriptive and consistent in style?
	* Are "magic numbers" avoided? (use named constants rather than numbers)
	* Is there any “dead code” (commented out code or unreachable code) that should be removed?
	* Is it possible to remove any of the assembly language code, if present?
	* Is the code too tricky? (Did you have to think hard to understand what it does?)
	* Did you have to ask the author what the code does? (code should be self-explanatory)
#. **Architecture**
	* Is the function too long? (e.g., longer than fits on one printed page)
	* Can this code be reused? Should it be reusing something else?
	* Is there minimal use of global variables? Do all variables have minimum scope?
	* Are classes and functions that are doing related things grouped appropriately? (cohesion)
	* Is the code portable? (especially variable sizes, e.g., “int32” instead of “long”)
	* Are specific types used when possible? (e.g., “unsigned” and typedef, not just "int")
	* Are there any if/else structures nested more than two deep? (consecutive “else if” is OK)
	* Are there nested switch or case statements? (they should never be nested)
#. **Exception Handling**
	* Are input parameters checked for proper values (sanity checking)?
	* Are error return codes/exceptions generated and passed back up to the calling function?
	* Are error return codes/exceptions handled by the calling function?
	* Are null pointers and negative numbers handled properly?
	* Do switch statements have a default clause used for error detection?
	* Are arrays checked for out of range indexing? Are pointers similarly checked?
	* Is garbage collection being done properly, especially for errors/exceptions?
	* Is there a chance of mathematical overflow/underflow?
	* Would an error handling structure such as try/catch be useful? (depends upon language)
#. **Timing**
	* Is the worst case timing bounded? (no unbounded loops, no recursion)
	* Are there any race conditions? (especially multi-byte variables modified by an interrupt)
	* Is appropriate code thread safe and reentrant?
	* Are there any long-running ISRs? Are interrupts masked for more than a few clocks?
	* Is priority inversion avoided or handled by the RTOS?
	* Is the watchdog timer turned on? Is the watchdog kicked only if every task is executing?
	* Has code readability been sacrificed for unnecessary optimization?
	 
#. **Validation and Testing**
	* Is the code easy to test? (how many paths are there through the code?)
	* Do unit tests have 100% branch coverage? (code should be written to make this easy)
	* Are the compilation and/or lint checks 100% warning-free? (are warnings enabled?)
	* Is special attention given to corner cases, boundaries, and negative test cases?
	* Does the code provide convenient ways to inject faulty conditions for testing?
	* Are all interfaces tested, including all exceptions?
 	* Has the worst case resource use been validated? (stack space, memory allocation)
	* Are run-time assertions being used? Are assertion violations logged?
	* Is there commented out code (for testing) that should be removed?
 
#. **Hardware**
	* Do I/O operations put the hardware in correct state?
	* Are min/max timing requirements met for the hardware interface?
	* Are you sure that multi-byte hardware registers can’t change during read/write?
	* Does the software ensure that the system resets to a well defined hardware system state?
	* Have brownout and power loss been handled?
	* Is the system correctly configured for entering/leaving sleep mode (e.g. timers)?
	* Have unused interrupt vectors been directed to an error handler?
	* Has care been taken to avoid EEPROM corruption? (e.g., power loss during write)

General Code Review
-------------------
*To do*
