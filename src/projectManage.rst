Project Management Basics
*************************

We will try to implement few principles from Agile and Scrum workflow (adapted for our application).

General
=======

Daily morning meeting
---------------------
9 am meeting for 15 minutes to discuss the following 

* What did you do yesterday?
* What will you do today?
* Are there any impediments?


Discussion sessions
-------------------

With very small teams in small spaces, there is a tendency to discuss issues with your colleague very frequently. This disrupts the flow. To avoid this, we have discussion slots. These are timed slots at specific times of the day for discussion. 

For example, there can be 2-3 slots in a day. 

* 11 - 11:30 am
* 2:30 - 3 pm
* 5:00 - 5:30 pm
* 7:30 - 8:00 pm

It is advised you have a list of questions you want to discuss. 


Communication
-------------

* For team communication, we use Slack. This is best used for a discussion, quick question etc. If you haven't received an invite already, please send an email to the admin. 


Email and Chat
--------------
* Email can be used when you have a quick announcement or something that does not really need the time and effort of an issue. 
* Chat can be used for a quick back and forth and also as a precursor to an issue. Maybe you had to consult someone before raising an issue or someone had talk to you about where and how to raise an issue. 

.. _onlineRepo:


Task Management 
===============

We will be using GitLab Issues and Issue boards to plan a project. 

* Each project has to be broken down into a list of tasks
* For example, if there is a project called *Mobile app testing*, it has to be broken down to specific test modules that have to be completed. 
* Using these tasks, GitLab issues have to be created in the corresponding repository and has to have an assignee.  
* Every project has its own GitLab repository. 
* If you are doing a set of tasks which are not big enough to be a project, please add them to the `General <https://gitlab.com/microgrid/general>`_ repository.
* To learn more about GitLab issues, see `here <https://docs.gitlab.com/ee/user/project/issues/>`_ 
* Issues have to be used used for bugs, product planning, documentation, new features etc. 
* You can raise and issue and assign it yourself. Submit the smallest item of work that makes sense
* If you raise and issue and its beyond your scope of your work, assign it to someone else.
* Make sure you have a time estimate for all your tasks using `time tracking <https://about.gitlab.com/solutions/time-tracking/>`_ on GitLab. 
* Do not leave issues open for long time. 
* **Please honor the due date mentioned in the issue, and if you need to extend it, please comment and justify the extension**. 



Issue labels
------------

Your GitLab repository should have the following labels

* current sprint - #8E44AD
* in-progress - #44AD8E
* on hold - #FF0000
* bug - #d9534f
* confirmed - #d9534f
* critical - #d9534f
* discussion - #428bca
* documentation - #f0ad4e
* enhancement - #5cb85c
* suggestion - #428bca
* support - #f0ad4e


Visualizing the tasks (Kanban)
------------------------------

* Task have to be visualized using the `Issue board <https://about.gitlab.com/product/issueboard/>`_. 
* By default there are two boards - *Open* and *Closed*
* Create the following lists (from your labels) and add issues to it
  * in-progress
  * on hold
  * current sprint


Create a Milestone 
------------------
Milestones are used for arranging issues into a determined group which can achieved within a specified amount of time by setting a start and due date. 

See GitLab `milestones <https://docs.gitlab.com/ce/user/project/milestones/>`_ and this `tutorial <https://www.tutorialspoint.com/gitlab/gitlab_milestones.htm>`_  to get started. 

Milestones can be used as Agile Sprints. 

**Milestones should have deadlines**. It is useless without a date.


Tracking time for tasks
-----------------------
* Track time for all your tasks using `time tracking <https://about.gitlab.com/solutions/time-tracking/>`_ on GitLab. 



Checklist
---------
* Divide a project into tasks 
* Enter tasks on GitLab using issues. 
* For each issue add assignee and time estimate
* Visualize tasks in issue board with 2 lists - *To do* and *Doing*
* Create a milestone. A bunch of issues constitute a milestones. A bunch of milestones lead to a product release. 
* Make sure you track time using GitLab time tracking, each time you spend time on a task.


Version Control and Online Repository
=====================================
* For version control we use Git and GitLab. GitLab also serves as our online repository (repo). 
* Each project has its own repo. If you do not have a repo for your project, send an email to the admin. 
* Repos are not restricted to hardware and software projects. They can be used for design documents, technical reports etc. 
* Refrain from creating new project repos unless you've been directed by the project lead.
* If you haven't used Git or GitLab or version control in general, a tutorial will be provided.



Drive Repo for planning and data dump
=====================================

* Every project usually has a repository on Google Drive for planning.
* This can be used for the following 
	* Preliminary planning 
	* Data dump
	* Confidential data  
* You can use Google Docs/Slides to create a rough draft/base version of a document is available, it should be converted to `Sphinx <http://www.sphinx-doc.org/>`_ and then moved to GitLab. Sphinx can then be used to generate a `LateX <https://www.latex-project.org/>`_ document.
* Once you convert the content to Sphinx, please add ``Deprecated:`` to the title of the Google Docs/Slides.
* Contact the admin/project lead for Drive repo details. 

.. _scheduling:

Scheduling
==========

* Before you start your project, please divide the project into number of tasks/modules. Estimate the number of hours/days it would take you to complete these tasks/modules. Double this number to get a more realistic number.
* Download `this <https://drive.google.com/open?id=1kWkvWwd7oVIanAdwNenHM4-0m0hLn36g-kHKo-dirpo>`_ template and use it for scheduling your project development. Make it as detailed as possible. 


Documentation Guidelines
========================
Project documentation is as important as the code, design etc. and must be done **hand-in-hand with the product development**. Read the section on (:ref:`doc`). 


Testing Guidelines
==================
Hardware and software testing **has to be done hand-in-hand with the product development**.  Read the section on (:ref:`testing`).


Before-you-start-development checklist
--------------------------------------
* You have access to the repository (:ref:`onlineRepo`). 
* You have made a list of project tasks and timeline (:ref:`scheduling`)
* You have written a base version of SRS (:ref:`srs`) / Functional Spec (:ref:`functionalSpec`) and/or Tech Specs (:ref:`techSpec`) in `Sphinx <http://www.sphinx-doc.org/>`_.
* Make sure you've setup your documentation tool (:ref:`doc`).
* Make sure you've your testing environment (:ref:`testing`).