Product Development
*******************

If you're designing new hardware or software product, app, etc., this section is for you. 

.. _srs:

System Requirement Specification(SRS)
=====================================

This document is created when designing a product and lays out the requirements of the system. This is mainly done for **hardware and embedded** projects. 

Planning
--------

* In the initial stages you can use a collaborative word processing software/text editor like Google Docs to plan out the SRS. It is not necessary to manage or add this in GitLab. 
* Once you have base version ready, this has to be converted to `LateX <https://www.latex-project.org/>`_ or `Sphinx <http://www.sphinx-doc.org/>`_ and added to the ``Doc/planning`` folder of your hardware repository (Sphinx can generate output in LateX). See :ref:`hardwareFile` for more details on file structure. 

SRS Document Outline and Content
--------------------------------

**Please use the following as a checklist when you're writing your SRS**. 
*Depending on the project, some of the following items might not be applicable.* 

[ ] Cover page (page 1)
^^^^^^^^^^^^^^^^^^^^^^^
* Mention that it is SRS
* List of authors
* Version number
* Date

[ ] Document Status (page 2)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
A table which contains the following fields

* Version no
* Issue date
* Amendment Details
* Modified By

[ ] TOC (Table of Contents)
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Page 3 with just the table of contents


[ ] Introduction
^^^^^^^^^^^^^^^^
This will contain the following sections

* *Purpose* - The Why
* *Scope* - identifies the product and application domain
* *Definitions, Acronyms, and Abbreviations* 
* *Reference* documents - list of reference documents used and where reader can find it
* Describe the *contents and structure of the remainder of SRS*

[ ] Overall Description
^^^^^^^^^^^^^^^^^^^^^^^
* *Product Overview* - how does the product work?
* *Major product Features*
* *Product Diagram* - block diagram of all components and their interconnections.
* *Constraints* - anything that will limit the developer options (e.g regulations, criticality, hardware/software limitations, parallelism etc.)

[ ] Specific Requirements
^^^^^^^^^^^^^^^^^^^^^^^^^
(each of these sub categories can be a two column (parameter and value) table )

* *Hardware* requirements - For example, this can include total number of channels, type and number of connectors, material, color, security etc. 
* *Electrical* requirements - this can include nominal and max current, system voltage, power consumption/production, energy usage, battery type etc. 
* *Mechanical and environmental* requirements - Enclosure size and weight, humidity, temperature, pressure requirements
* *Measurements and actuation* - if you're system is responsible for taking measurements or actuation, please mention that here. 
* *Embedded computing* system requirements - if you're hardware system using micro controllers or processors, please mention its requirements. This can include 
    * Controller/processor architecture
    * Memory requirements
    * Storage
    * Data logging capabilities 
* *Embedded software system* requirements
    * Critical processes to be performed by the computing system (basic upkeep)
    * Non-critical processes to be performed by the computing system (basic upkeep)
    * Functional tasks to be performed by the software
    * Interaction of the software with the user through APIs 
    * Interaction of the software with other hardware/software entities
* *Quality and performance* - What are the requirements for speed, availability, response time, recovery time of various hardware and software functions. 
* *Enclosure* - does your product need an enclosure? (the look and feel of the product is important for increased consumer acceptance and participation.) 
    * What is the shape, size, and color of the enclosure?
    * What is the material?
    * Does it have to meet any IP (Ingress Protection or International Protection) standards?
    * Is it a standard enclosure than you can buy off-the-shelf? Do you need to make modifications to it?
    * Does it have to be custom designed? If yes, answer the following questions 
        * Can it be 3D printed? If no, provide other options?
        * Are you going to design the enclosure or is there an external entity responsible for this?
        * Who is responsible for writing the SRS for the enclosure which contains the following info?
        * What is the shape, size, and color?
        * How much weight should it sustain?
        * Number of holes, type, size, and placement?
        * Does your product have to be secured inside? If so, how?
* *Safety and Standards*
    * Should your system meet any specific standards - IEC/UL/FCC. Some examples include 
    * IEC60950-I
    * IEC61010
    * IEC62053-21 Class 1, 
    * FCC part 15
    * UL 2735, 
    * ANSI C12.10 - this includes the physical aspects and safety of watthour meters
    * If you do not have this info, can you comment on the following 
        * Components
        * Compliance with component standards
        * Usage within their recognized ratings
        * These can include plastics, PCBs, wire, transformers etc. 
        * Electrical
        * Accessibility of hazardous live parts
        * Provisions for earthing 
        * Isolation Circuitry
        * Fire
        * Enclosure Flammability
        * Battery protection, charging, placement, and replacement
        * Mechanical
        * Static
        * Drop
        * Impact

[ ] Additional Info 1 - Development Tools
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    * What software ecosystem/tools? What version?
    * Version control
    * Any specific protocols?
    * Any preferred operating system?

[ ] Additional Info 2 - Estimate BOM. 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Prepare a rough but realistic cost per unit. Add the following details 

    * Estimate cost per unit and break it down in terms of manufacturing, assembly, labor etc. 
    * Add breakdown for subsystem - communication, computing, power supply, isolation etc. 
    * Estimate cost per 100 and 1000 units. 


-------

.. _functionalSpec:

Functional Specifications
=========================

This document is created when designing a product and lays out the functional specs of the system. This is mainly done for **software** and some *hardware embedded* projects. The functional specification goes hand-in-hand with the technical specification.

Planning
--------
* In the initial stages you can use a collaborative word processing software/text editor like Google Docs to plan out the functional specifications. It is not necessary to manage or add this on GitLab. 
* Once you have base version ready, this has to be converted to `LateX <https://www.latex-project.org/>`_ or `Sphinx <http://www.sphinx-doc.org/en/master/document>`_ and added to the ``doc\planning`` folder of your repository on GitLab. See :ref:`softwareFile` for more details on file structure. 

Functional Specs Outline and Content
------------------------------------
* Cover page and list of authors (page 1)
* Document status (page 2) - a table which contains the following fields
    * Version no
    * Issue date
    * Amendment Details
    * Modified By
* Table of contents
* Introduction. This will contain the following sections
    * *Purpose* - The Why
    * *Scope* - identifies the product and application domain
    * *Definitions, Acronyms, and Abbreviations* 
    * *Reference* documents - list of reference documents used and where reader can find it
    * Describe the *contents and structure of the remainder of the functional specs*
* Specs
    * Product Overview - how does the product work?
    * Major features
    * What's the design goal? See `example 1 <https://www.ucl.ac.uk/indigo/design-foundation/design-goals>`_ and `example 2 <https://www.ucl.ac.uk/indigo/design-foundation/design-goals>`_.
    * Rollout - when and how?
    * Major components 
    * Overview of software architecture
    * User interaction and experience 
    * Look and feel
* Flow of the product
    * User interaction flow 
    * Integration of 3rd party apps 

-------

.. _techSpec:

Technical Specifications
========================

*To do*

-------



Things to consider 
==================


When working on an embedded project
-----------------------------------
* **Modularity**
    * The hardware code should be cleanly separated from the logic code so that unit testing can be done easily. 
* **Software/firmware**
    * Your embedded device should not have the source code
    * It should only have the binary or executable 
* **Logging** 
    * Make sure you're logging system info - this can be system restarts, updates, loss of communication, errors etc. This should be logged onto your on-board storage
* **Power consumption** of your embedded controller
    * Whether this is a micro-controller or a processor, please have good estimates of its power consumption (through measurement)
* **Power quality** for onboard electronics
    * Your on-board electronics (especially your processors and controllers ) require high quality power and are very sensitive to under and over voltage. Some processors have certain dc power ramp up requirements or the power management system goes into a lockup state. `See this example for BeagleBone Black <https://elinux.org/BeagleBone_Power_Management>`_. Make sure you're using capacitors for filtering and for providing short bursts of energy. Communication modules face similar issues. 
* **Updating your firmware/software** - have a plan for updating your software (securely) remotely (if you have communication) or manually. 
* **Check availability of ICs** - make sure none of your ASICs are nearing end-of-life and are readily available from major vendors such as Element14, Digikey, Newark etc.
