.. _testing:

Testing
*******


Software Testing
================
*Work in Progress*


Using continuous integration (CI) for builds and testing
--------------------------------------------------------

* `Why CI? <https://about.gitlab.com/2015/12/14/getting-started-with-gitlab-and-gitlab-ci/>`_. 
* CI is the process of integrating new changes to the code as when they it is developed instead of waiting for milestones. Each commit to the development repository will be followed by two steps
 	* Build (cross-build) of the application (including auto-generation of code documentation)
 	* Run unit and integration tests to check if code is valid 
* We will be using `GitLab CI <https://docs.gitlab.com/ee/ci/>`_

Embedded Software Builds
^^^^^^^^^^^^^^^^^^^^^^^^
* Building embedded projects outside the target hardware might be a little tricky. 
* We will be using the necessary `toolchain <https://launchpad.net/gcc-arm-embedded>`_ to cross-compile code for ARM Cortex-M0/1/3/4 devices. 
* We will use these tools on GitLab runners
* References
	* `CI for embedded devices <https://github.com/kylemanna/docker-ci-embedded>`_. 
	* `CMSIS for Atmel SAM series discussion <https://www.avrfreaks.net/forum/how-get-cmsis-gcc-and-samd2x-and-samr21-under-linux>`_.
	* `CI for embedded systems <https://jamesmunns.com/blog/hardware-ci-overview/>`_.


Embedded Software Testing
-------------------------


What is it?
^^^^^^^^^^^
* Embedded software testing - https://www.archive.ece.cmu.edu/~ece649/lectures/10_testing.pdf


General
^^^^^^^

* As a general overview, we will try to test all application logic and as much hardware interaction software as possible. The compile/build process will be tested as per the last section. 
* Wherever possible we will mock the HAL. 
* We will be using a test framework `cmocka <https://cmocka.org/>`_ along with custom tests. 
* cmocka can be used to test interface dependencies (say I2C) using `mock objects <https://lwn.net/Articles/558106/>`_.
* All the software tests will be part of our CI process on GitLab. 
* References 
	* `Unit testing embedded code with cmocka <http://www.samlewis.me/2016/09/embedded-unit-testing-with-cmocka/>`_
	* `Unit testing with mock objects in C <https://lwn.net/Articles/558106/>`_.
	* `Unit testing and mocking with cmocka <https://sambaxp.org/fileadmin/user_upload/sambaXP2018-Slides/schneider-sambaxp_2018_andreas_schneider_cmocka.pdf>`_ presentation
	* `Test Driven Development for Embedded C <https://sambaxp.org/fileadmin/user_upload/sambaXP2018-Slides/schneider-sambaxp_2018_andreas_schneider_cmocka.pdf>`_
	* `Unit testing embedded C applications with Ceedling <https://dmitryfrank.com/articles/unit_testing_embedded_c_applications>`_. This uses a different framework but the principles are the same. 



[Imp] cmocka tutorials
^^^^^^^^^^^^^^^^^^^^^^

* https://lwn.net/Articles/558106/
* https://blog.cryptomilk.org/2013/01/14/cmocka-a-unit-testing-framework-for-c/
* http://zhuyong.me/blog/2014/03/19/c-code-unit-testing-using-cmocka/
* https://github.com/SurjitSahoo/cmocka
* https://blog.microjoe.org/2017/unit-tests-c-cmocka-coverage-cmake.html
* https://riptutorial.com/c/example/32344/cmocka
* https://re-ws.pl/2018/10/using-cmocka-for-unit-testing-c-code/
* https://lonewolfer.wordpress.com/2014/10/13/test-driven-c-cmake-cmocka-complete-code-coverage/




CI checklist
^^^^^^^^^^^^
* Code 
* Test framework. In our case, it is `cmocka <https://cmocka.org/>`_. 
* A CI tool. In our case, it is GitLab CI. See here for `A beginner's guide to CI on GitLab <https://about.gitlab.com/2018/01/22/a-beginners-guide-to-continuous-integration/>`_. 
	* You need a ``.gitlab-ci.yml`` file. Get a template from `here <https://gitlab.com/gitlab-org/gitlab-ce/tree/master/lib/gitlab/ci/templates>`_.
	* Figure out the `stages <https://docs.gitlab.com/ee/ci/yaml/#stages>`_ in the `pipeline <https://docs.gitlab.com/ee/ci/yaml/#pipelines>`_.  For example, there can be two stages - ``build`` and ``test``. See  for more indo. This way if ``build`` stages does not execute successfully, the ``test`` stage is not deployed. 
	* When a pipeline fails, the developers will be notified in an email. However, we need more info. Usually test units output generate a ``XMl`` report. You should store it in the ``test/build`` directory. *to do* - How is this displayed in GitLab because its unlikely the XML document is human readable. `See this <https://docs.gitlab.com/ee/ci/junit_test_reports.html>`_.
	* You need to add these badges - ``pipeline`` and ``coverage``. 



CI flow 
-------
*To do*

Hardware Testing
================
*To do*

