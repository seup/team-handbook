.. _doc:

Documentation
*************


General Documentation Guidelines
================================
* We will be using `reStructuredText <http://docutils.sourceforge.net/rst.html>`_ for markup syntax (we can say rST is a language) and `Sphinx <http://www.sphinx-doc.org/>`_ for processing rST. 
* Sphinx will serve as our **primary documentation tool for creating documents** and also **code documentation (auto and manual)** 
* Sphinx is free and open-source. Sphinx is/was primarily developed for Python but has been integrated with other languages through plugins and bridges. We will be using these when working with non-Python languages. 
* `Why Sphinx? <https://varnish-cache.org/docs/trunk/phk/sphinx.html>`_. Sphinx can be used to generate outputs in various formats (including LateX)
* A PDF version of this manual can be found `here <https://gitlab.com/seup/team-handbook/blob/master/TeamHandbook.pdf>`_.

When to use Sphinx? (document creation)
---------------------------------------
Sphinx can be used for any document that is 

* Not a temporary repository of data 
* Has to be uploaded on the web (internal or external) 
* Collaborative document

*Examples include*

* Requirement/Functional/Tech specifications
* Surveys 
* Technical manuals
* Operational manuals
* Planning materials 
* Safety data sheets


**NOTE**: Do not use more than 3 nesting levels for lists. More than 3 leads to to `deeply nested error <https://texfaq.org/FAQ-toodeep>`_ when generating a PDF document using LateX. 

Using Sphinx for code documentation
-----------------------------------
We will follow the two step rule for code documentation

* **Step 1**- We will be using Sphinx (or its extensions/plugins/bridges) to auto-generate documentation from inline comments 
* **Step 2** - To the above, we will add manual documentation to give it more context 


Code Documentation Tools
------------------------

All documentation outputs should be in HTML and PDF formats and should be stored in the ``doc`` directory of your project. Here are the list of tools we will be using

* C/C++ :  `Doxygen <http://www.doxygen.nl/>`_ + `Breathe <https://breathe.readthedocs.io>`_. Examples of Breathe can be found on its `repository <https://github.com/michaeljones/breathe>`_.
* Python:  `Sphinx <http://www.sphinx-doc.org/>`_.
* JavaScript: `JSDocs <http://usejsdoc.org/>`_  + `sphinx-js <https://github.com/erikrose/sphinx-js>`_


-----


.. _docHardware:

Documenting Hardware Schematics
===============================
Please make sure you have the following for your schematics. This will not only help the readers but also  the future developers and engineers.


* Table of contents (main page)
* Revision history - have an area where we can enter that. As of now, you have now revision history. (main page)
* Have a box for contributing authors
* Have a box for drawn by and approved by (main page)
* Have sheet number and title (every page)
* Have a table for revision.
* Block diagram for entire system (page 2)
* Have a box for design notes (every page). The design notes give info about design requirements and decisions. For example a design note would be - this unit is used to convert 5-60V dc to 5v dc for onboard electronics or giving details about the fuse.
* Have a box for layout notes (every page). The layout notes give info regarding the layout requirements. For example, a layout note would be - place decoupling capacitors near supply.
* A sheet for incomplete or remaining work - if the schematics/board design are incomplete, please mention in detail here so that new developers can work on this.
* Make sure every component in the schematic has few mandatory properties  and make sure this is consistent among all the components. [There is a way to get this automatically] (https://www.altium.com/documentation/18.0/display/ADES/((Searching+for+Components))_AD). You should include (these values can be obtained from DigiKey part description). Feel free to add extra parameters like voltage, current, temperature etc.
	* Category
	* ComponentLink1Description
	* ComponentLink1URL
	* Manufacturer
	* Manufacturer Part Number
	* Pricing 1
	* Supplier 1
	* Supplier Part Number 1
	* Supplier Device Package

------




Documenting Embedded Code
=========================

* Generally, use comments to tell WHAT your code does and WHY, not HOW
* Document code such that it can be auto generated. We will be using `Doxygen <http://www.doxygen.nl/>`_ + `Breathe <https://breathe.readthedocs.io/en/latest/quickstart.html>`_ for C/C++. Your output can be PDF or HTML and should be in the ``doc`` directory. Breathe provides a bridge between Sphinx and Doxygen documentation system.
* Tutorial for Doxygen can be found in `1 <http://flcwiki.desy.de/How%20to%20document%20your%20code%20using%20doxygen>`_ and `2 <http://merlot.usc.edu/cs102-s11/doxygen/>`_.
* Quick start guide for Breathe can be found `here <https://breathe.readthedocs.io/en/latest/quickstart.html>`_.
* **Avoid writing obvious comments**. `See this <http://geekandpoke.typepad.com/geekandpoke/2011/02/the-real-coder.html>`_.
* Document any hacks, quirks or clear techniques which you think would be non-obvious.
* Add long (multi-line) comments at the beginning of a file/function/class to explain what it does and possibly why. ::

	/*
	 * This is the preferred style for multi-line
	 * comments in the Linux kernel source code.
	 * Please use it consistently.
	 *
	 * Description:  A column of asterisks on the left side,
	 * with beginning and ending almost-blank lines.
	 */

* Every file must have a file header documentation which contains the name of the file, basic description, author and date ::

	/**
	* @file util.h
	* @brief this header file will contain all required
	* definitions and basic utilities functions.
	*
	* @author Dr. ABC
	*
	* @date 4/8/2018
	*/

* All functions must have a function header documentation. ::

	/**
	* This method will be used to print a single character to the lcd.
	* @author Dr.ABC
	* @param chrData The character to print
	* @date 4/8/2018
	*/

