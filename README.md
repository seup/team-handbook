![Build Status](https://gitlab.com/seup/team-handbook/badges/master/pipeline.svg)

---

Contains the source files for SEUP team handbook (STH). The SEUP team handbook is a central repository for how we manage our projects. SEUP stands for [Simple Electric Utility Platform](https://seupdocs.readthedocs.io). SEUP is a project at the University of Wisconsin-Madison where we are building tools for energy management and education.

HTML version of the handbook can be found [here](https://seup.gitlab.io/team-handbook/)

PDF version of the handbook can be found [here](https://gitlab.com/seup/team-handbook/blob/master/TeamHandbook.pdf)


## Why STH?

* More professional approach to product development and maintenance
* Follow good industry practices 
* Better project management
* Good documentation for users, current and future developers
* Aligning project for potential community (or large team) collaboration 
* Increase product reliability and performance through better pre (definition and requirement specs) and post (testing) product development practices.
* Allow addition of ``CI/CD``. 

## Current Version

0.1.0

## LICENSE

IT can be found [here](LICENSE.md)

## CONTRIBUTING GUIDE

It can be found [here](CONTRIBUTING.md)

## CHANGELOG

It can be found [here](CHANGELOG.md)

## BUGS 

It can be found [here](BUGS.md)

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
2. Install [Sphinx](http://www.sphinx-doc.org/) and [LateX](http://www.sphinx-doc.org/).
3. Generate HTML documentation: `make html`. The generated HTML will be located in the location specified by `conf.py`, in this case `_build/html`.
4. Generate PDF documentation: `make latexpdf`. The generated LateX files and PDF will be located in the location specified by `conf.py`, in this case `_build/latex`.

## Forking for own use?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

---

Forked from https://gitlab.com/Eothred/sphinx
