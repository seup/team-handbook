# Contributing to this Document 
Thanks for your interest in contributing to the SEUP handbook. This guide details how to contribute to this document. 

**There are different ways to contribute**

1. If you find a bug, please raise an issue [here](https://gitlab.com/seup/team-handbook/issues) first. Then create a merge request [here](https://gitlab.com/seup/team-handbook/merge_requests). 
2. You can also check the issue section to find issues with a tag `Accepting merge requests`. These are usually very specific problems you can work on. 
3. More open ended problems will be listed can be found [here](#other-things-you-can-work-on)


##### Other things you can work on

*Coming soon*
